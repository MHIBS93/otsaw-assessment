<?php

namespace otsaw_assessment\Http\Controllers;

use Illuminate\Http\Request;

use otsaw_assessment\Http\Requests;
use otsaw_assessment\Http\Controllers\Controller;

use DB;

class EmployeeDeleteController extends Controller
{
    public function destroy(Request $request) {
        $id = intval($request->input('d_employee_id'));
        
        DB::delete('DELETE FROM employee WHERE employee_id = ?', [$id]);
        
        header("Location: /employee_index");
        die();
    }
    
    public function destroyall() {
        DB::delete('DELETE FROM employee');
        
        header("Location: /employee_index");
        die();
    }
}
