<?php

namespace otsaw_assessment\Http\Controllers;

use Illuminate\Http\Request;

use otsaw_assessment\Http\Requests;
use otsaw_assessment\Http\Controllers\Controller;

use DB;

class EmployeeInsertController extends Controller
{
    
    public function insert(Request $request) {
        $name = $request->input('a_employee_name');
        $email = $request->input('a_employee_email');
        $contactNo = intval($request->input('a_employee_contact_number'));
        $address = $request->input('a_employee_address');
        $postalCode = intval($request->input('a_employee_postal_code'));
        
        DB::insert('INSERT INTO employee (employee_name, employee_email, employee_contact_number, employee_address, employee_postal_code) VALUES (?, ?, ?, ?, ?)', [$name, $email, $contactNo, $address, $postalCode]);
        
        header("Location: /employee_index");
        die();
    }
}
