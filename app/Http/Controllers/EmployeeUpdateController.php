<?php

namespace otsaw_assessment\Http\Controllers;

use Illuminate\Http\Request;

use otsaw_assessment\Http\Requests;
use otsaw_assessment\Http\Controllers\Controller;

use DB;

class EmployeeUpdateController extends Controller
{
    
    public function edit(Request $request) {
        $id = intval($request->input('u_employee_id'));
        $name = $request->input('u_employee_name');
        $email = $request->input('u_employee_email');
        $contactNo = intval($request->input('u_employee_contact_number'));
        $address = $request->input('u_employee_address');
        $postalCode = intval($request->input('u_employee_postal_code'));
        
        DB::update('UPDATE employee SET employee_name = ?, employee_email = ?, employee_contact_number = ?, employee_address = ?, employee_postal_code = ? WHERE employee_id = ?', [$name, $email, $contactNo, $address, $postalCode, $id]);
        
        header("Location: /employee_index");
        die();
    }
}
