<?php

namespace otsaw_assessment\Http\Controllers;

use Illuminate\Http\Request;

use otsaw_assessment\Http\Requests;
use otsaw_assessment\Http\Controllers\Controller;

use DB;

class EmployeeViewController extends Controller
{
    public function index() {
        $employees = DB::table('employee')->paginate(5);
        return view('EmployeeIndex', ['employees' => $employees]);
    }
}
