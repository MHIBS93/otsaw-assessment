<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () { return view('welcome'); });
Route::get('/employee_index', 'EmployeeViewController@index');
Route::post('/employee_insert', 'EmployeeInsertController@insert');
Route::post('/employee_update', 'EmployeeUpdateController@edit');
Route::post('/employee_delete', 'EmployeeDeleteController@destroy');
Route::get('/employee_deleteAll', 'EmployeeDeleteController@destroyall');