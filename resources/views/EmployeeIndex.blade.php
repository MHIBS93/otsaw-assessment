<html>

    <head>
        <title>Employee Records</title>
        
        <!-- Any external scripts here -->
        <script
            src="https://code.jquery.com/jquery-3.2.1.js"
            integrity="sha256-DZAnKJ/6XZ9si04Hgrsxu/8s717jcIzLy3oi35EouyE="
            crossorigin="anonymous">
        </script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.13/js/dataTables.bootstrap.min.js"></script>
        
        <!-- Any external stylesheets here -->
        <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.10.13/css/dataTables.bootstrap.min.css">
    </head>
    
    <body>
        
        <h1>Employees Information</h1>
        
        <br>
        
        <table>
            <tr>
                <td><button class="btn btn-info btn-lg" data-toggle="modal" data-target="#addModal">Add New Employee</button></td>
                <td><button class="btn btn-info btn-lg" data-toggle="modal" data-target="#deleteAllModal">Delete All</button></td>
            </tr>
        </table>
        
        <br>
        
        <table id="employees" class="table table-striped table-bordered" cellspacing="0" width="100%">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Contact Number</th>
                    <th>Address</th>
                    <th>Postal Code</th>
                </tr>
            </thead>
            <tbody>
            @foreach($employees as $employee)
                <tr>
                    <td>{{ $employee->employee_name }}</td>
                    <td>{{ $employee->employee_email }}</td>
                    <td>{{ $employee->employee_contact_number }}</td>
                    <td>{{ $employee->employee_address }}</td>
                    <td>{{ $employee->employee_postal_code }}</td>
                    <td><button class="updateModalDialog btn btn-info btn-sm" data-id="{{ $employee->employee_id }}" data-name="{{ $employee->employee_name }}" data-email="{{ $employee->employee_email }}" data-contact_number="{{ $employee->employee_contact_number }}" data-address="{{ $employee->employee_address }}" data-postal_code="{{ $employee->employee_postal_code }}" data-toggle="modal" data-target="#updateModal">Update</button></td>
                    <td><button class="deleteModalDialog btn btn-info btn-sm" data-id="{{ $employee->employee_id }}" data-toggle="modal" data-target="#deleteModal">Delete</button></td>
                </tr>
            @endforeach
            </tbody>
            <?php echo $employees->render(); ?>
        </table>
        
        <div class="modal fade" id="addModal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="/employee_insert" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4>Add Employee</h4>
                        </div>
                        <div class="modal-body">
                            <table id="updateTable" cellspacing="0" width="70%">
                                <tbody>
                                    <tr>
                                        <td>Employee Name:</td>
                                        <td><input id="a_employee_name" type="text" name="a_employee_name"></td>
                                    </tr>
                                    <tr>
                                        <td>Employee Email:</td>
                                        <td><input id="a_employee_email" type="text" name="a_employee_email"></td>
                                    </tr>
                                    <tr>
                                        <td>Employee Contact No:</td>
                                        <td><input id="a_employee_contact_number" type="text" name="a_employee_contact_number"></td>
                                    </tr>
                                    <tr>
                                        <td>Employee Address:</td>
                                        <td><input id="a_employee_address" type="text" name="a_employee_address"></td>
                                    </tr>
                                    <tr>
                                        <td>Employee Postal Code:</td>
                                        <td><input id="a_employee_postal_code" type="text" name="a_employee_postal_code"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-info btn-sm">Insert</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
        <div class="modal fade" id="updateModal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="/employee_update" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4>Update Employee Details</h4>
                        </div>
                        <div class="modal-body">
                            <input id="u_employee_id" type="hidden" name="u_employee_id">
                            <table id="updateTable" cellspacing="0" width="70%">
                                <tbody>
                                    <tr>
                                        <td>Employee Name:</td>
                                        <td><input id="u_employee_name" type="text" name="u_employee_name"></td>
                                    </tr>
                                    <tr>
                                        <td>Employee Email:</td>
                                        <td><input id="u_employee_email" type="text" name="u_employee_email"></td>
                                    </tr>
                                    <tr>
                                        <td>Employee Contact No:</td>
                                        <td><input id="u_employee_contact_number" type="text" name="u_employee_contact_number"></td>
                                    </tr>
                                    <tr>
                                        <td>Employee Address:</td>
                                        <td><input id="u_employee_address" type="text" name="u_employee_address"></td>
                                    </tr>
                                    <tr>
                                        <td>Employee Postal Code:</td>
                                        <td><input id="u_employee_postal_code" type="text" name="u_employee_postal_code"></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-info btn-sm">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
        <div class="modal fade" id="deleteModal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form action="/employee_delete" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4>Delete Employee</h4>
                        </div>
                        <div class="modal-body">
                            <input id="d_employee_id" type="hidden" name="d_employee_id">
                            <p>Are you sure that you want to delete this employee from the database?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-info btn-sm">Delete</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        
        <div class="modal fade" id="deleteAllModal" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4>Delete All Employee</h4>
                    </div>
                    <div class="modal-body">
                        <p>Are you sure that you want to delete all employees from the database?</p>
                    </div>
                    <div class="modal-footer">
                        <a class="btn btn-info btn-sm" href="/employee_deleteAll">Delete All</a>
                    </div>
                </div>
            </div>
        </div>
    </body>
    
    <script>
        $(document).on("click", ".updateModalDialog", function () {
            var id = $(this).data('id');
            var name = $(this).data('name');
            var email = $(this).data('email');
            var contactNumber = $(this).data('contact_number');
            var address = $(this).data('address');
            var postalCode = $(this).data('postal_code');
            
            $(".modal-body #u_employee_id").val(id);
            $(".modal-body #u_employee_name").val(name);
            $(".modal-body #u_employee_email").val(email);
            $(".modal-body #u_employee_contact_number").val(contactNumber);
            $(".modal-body #u_employee_address").val(address);
            $(".modal-body #u_employee_postal_code").val(postalCode);
        });
        
        $(document).on("click", ".deleteModalDialog", function () {
            var id = $(this).data('id');
            
            $(".modal-body #d_employee_id").val(id);
        });
    </script>
    
</html>